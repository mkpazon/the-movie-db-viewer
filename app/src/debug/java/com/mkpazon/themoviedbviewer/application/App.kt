package com.mkpazon.themoviedbviewer.application

import com.facebook.stetho.Stetho
import timber.log.Timber

class App : AbstractApp() {

    override fun onCreate() {
        super.onCreate()
        initTimber()

        initStetho()
    }


    private fun initTimber() {
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return String.format("[%s#%s:%s]", super.createStackElementTag(element), element.methodName, element.lineNumber)
            }
        })
    }

    private fun initStetho() {
        Stetho.initializeWithDefaults(this)
    }


}