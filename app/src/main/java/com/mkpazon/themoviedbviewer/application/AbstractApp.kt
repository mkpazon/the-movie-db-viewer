package com.mkpazon.themoviedbviewer.application

import android.app.Application
import com.mkpazon.themoviedbviewer.dagger.AppComponent
import com.mkpazon.themoviedbviewer.dagger.DaggerAppComponent
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration

abstract class AbstractApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initUIL()
    }

    private fun initUIL() {
        val defaultDisplayOptions = DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build()

        val config = ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultDisplayOptions)
                .build()

        ImageLoader.getInstance().init(config)
    }


    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().build()
    }
}