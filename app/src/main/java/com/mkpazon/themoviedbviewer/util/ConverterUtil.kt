package com.mkpazon.themoviedbviewer.util

import com.mkpazon.themoviedbviewer.model.Movie
import com.mkpazon.themoviedbviewer.model.network.MovieResult

object ConverterUtil {
    fun toMovie(movieResponse: List<MovieResult>?): List<Movie> {
        val movies = mutableListOf<Movie>()
        movieResponse.let {
            for(movieItem in it!!) {
                movies.add(Movie(movieItem.title, "https://image.tmdb.org/t/p/w300/${movieItem.posterPath}", movieItem.voteAverage))
            }
        }
        return movies
    }
}