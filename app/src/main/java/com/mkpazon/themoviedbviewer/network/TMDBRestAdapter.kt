package com.mkpazon.themoviedbviewer.network

import com.mkpazon.themoviedbviewer.model.network.GetMovieResponse
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject


private const val FIELD_API_KEY = "api_key"

class TMDBRestAdapter @Inject constructor(retrofit: Retrofit) {
    private val tmdbService: TMDBService

    interface TMDBService {

        @GET("discover/movie?sort_by=popularity.desc")
        fun getMostPopularMovies(@Query("page") page: Int): Flowable<Response<GetMovieResponse>>
    }

    fun getMostPopularMovies(page: Int): Flowable<Response<GetMovieResponse>> {
        return tmdbService.getMostPopularMovies(page)
    }

    init {
        tmdbService = retrofit.create(TMDBService::class.java)
    }
}