package com.mkpazon.themoviedbviewer.dagger

import com.mkpazon.themoviedbviewer.ui.home.HomePresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {
    @Provides
    @Singleton
    fun provideHomePresenter() = HomePresenter(mutableListOf())
}