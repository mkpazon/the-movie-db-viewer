package com.mkpazon.themoviedbviewer.dagger

import com.mkpazon.themoviedbviewer.ui.home.HomeFragment
import com.mkpazon.themoviedbviewer.ui.home.HomePresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [PresenterModule::class, NetworkModule::class])
interface AppComponent {
    fun inject(target: HomeFragment)
    fun inject(target: HomePresenter)
}