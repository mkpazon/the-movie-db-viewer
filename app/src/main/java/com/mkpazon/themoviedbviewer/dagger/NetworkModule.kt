package com.mkpazon.themoviedbviewer.dagger

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.mkpazon.themoviedbviewer.BuildConfig
import com.mkpazon.themoviedbviewer.network.TMDBRestAdapter
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Singleton

private const val FIELD_API_KEY = "api_key"

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRestAdapter(retrofit: Retrofit): TMDBRestAdapter {
        return TMDBRestAdapter(retrofit)
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, objectMapper: ObjectMapper): Retrofit {
        return Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.API_ROOT_URL)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideObjectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
        return objectMapper
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(): OkHttpClient {
        val okhttpBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            okhttpBuilder.addNetworkInterceptor(StethoInterceptor())
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okhttpBuilder.addInterceptor(httpLoggingInterceptor)
        }

        okhttpBuilder.addInterceptor { chain ->
            val request = chain.request()

            // Include the api key in all requests`
            val url = request.url().newBuilder().addQueryParameter(FIELD_API_KEY, BuildConfig.MOVIEDB_API_KEY).build()
            val newRequest = request.newBuilder().url(url).build()

            chain.proceed(newRequest)
        }
        return okhttpBuilder.build()
    }
}