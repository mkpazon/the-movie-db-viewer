package com.mkpazon.themoviedbviewer.model.network

class MovieResult {
    var title: String = ""
    var id: Int = 0
    var voteAverage: Float = 0.0F
    var posterPath: String? = null

}