package com.mkpazon.themoviedbviewer.model

data class Movie(val title: String, val imageUrl: String?, val rating: Float)
