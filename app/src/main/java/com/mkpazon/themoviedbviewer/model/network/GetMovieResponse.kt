package com.mkpazon.themoviedbviewer.model.network

class GetMovieResponse {
    var totalResults: Int = 0
    var totalPages: Int = 0
    var results: List<MovieResult>? = null
}