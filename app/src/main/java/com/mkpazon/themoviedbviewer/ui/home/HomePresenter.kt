package com.mkpazon.themoviedbviewer.ui.home

import com.mkpazon.themoviedbviewer.model.Movie
import com.mkpazon.themoviedbviewer.model.network.GetMovieResponse
import com.mkpazon.themoviedbviewer.network.TMDBRestAdapter
import com.mkpazon.themoviedbviewer.util.ConverterUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject

class HomePresenter(val mMovies: MutableList<Movie>) {

    @Inject
    lateinit var restAdapter: TMDBRestAdapter

    private var mView: HomeView? = null
    private var mPage = 1

    fun setView(homeView: HomeView) {
        mView = homeView
    }

    fun loadMovies() {
        Timber.d(".loadMovies")
        restAdapter.getMostPopularMovies(mPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    mView?.showProgressView(true)
                }
                .doOnTerminate({
                    mView?.showProgressView(false)
                })
                .subscribeWith(object: DisposableSubscriber<Response<GetMovieResponse>>(){
                    override fun onComplete() {
                        Timber.d(".onComplete")
                    }

                    override fun onNext(response: Response<GetMovieResponse>) {
                        Timber.d(".onNext")
                        if (response.isSuccessful) {
                            val movieResponse = response.body()
                            Timber.i("Size is:" + movieResponse?.results?.size)
                            val movies = ConverterUtil.toMovie(movieResponse?.results)
                            mMovies.addAll(movies)
                            mView?.updateList()
                            mPage++
                        } else {
                            response.errorBody().let {
                                Timber.w("A problem has occurred: $it")
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(e, ".onError")
                    }

                })
    }

    fun onSwipeRefresh() {
        mMovies.clear()
        mView?.resetScrollListener()
        mPage = 1
        loadMovies()
    }
}