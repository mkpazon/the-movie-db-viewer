package com.mkpazon.themoviedbviewer.ui.home

interface HomeView {
    fun showProgressView(show: Boolean)
    fun updateList()
    fun resetScrollListener()
}