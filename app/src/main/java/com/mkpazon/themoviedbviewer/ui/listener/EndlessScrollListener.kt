package com.mkpazon.themoviedbviewer.ui.listener

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import timber.log.Timber

abstract class EndlessRecyclerOnScrollListener(
        private val visibleThreshold: Int,
        private val mLinearLayoutManager: LinearLayoutManager)
    : RecyclerView.OnScrollListener() {

    private var previousTotal = 0 // The total number of items in the dataset after the last load
    private var loading = true // True if we are still waiting for the last set of data to load.
    private var firstVisibleItem: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0

    private var currentPage = 0

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        visibleItemCount = recyclerView!!.childCount
        totalItemCount = mLinearLayoutManager.itemCount
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }


        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            // End has been reached
            Timber.d("End has been reached")
            Timber.d("loading more items")

            // Do something
            currentPage++
            onLoadMore(currentPage)
            loading = true
        }
    }

    fun reset() {
        previousTotal = 0
        currentPage = 0
    }

    abstract fun onLoadMore(currentPage: Int)
}