package com.mkpazon.themoviedbviewer.ui.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mkpazon.themoviedbviewer.R
import com.mkpazon.themoviedbviewer.application.App
import com.mkpazon.themoviedbviewer.ui.adapter.PopularMoviesAdapter
import com.mkpazon.themoviedbviewer.ui.listener.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import javax.inject.Inject


class HomeFragment : Fragment(), HomeView {

    @Inject lateinit var mPresenter: HomePresenter
    private lateinit var mAdapter: PopularMoviesAdapter

    private lateinit var mScrollListener: EndlessRecyclerOnScrollListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Initialize RecyclerView
        val recyclerView = view!!.recyclerViewMovies
        val layoutManager = GridLayoutManager(activity, 2)
        recyclerView.layoutManager = layoutManager
        mAdapter = PopularMoviesAdapter(mPresenter.mMovies)
        recyclerView.adapter = mAdapter

        initScrollListener(layoutManager)
        recyclerView.addOnScrollListener(mScrollListener)

        view.swipeRefereshLayout.setOnRefreshListener { mPresenter.onSwipeRefresh() }
        return view
    }

    private fun initScrollListener(layoutManager: LinearLayoutManager) {
        mScrollListener = object : EndlessRecyclerOnScrollListener(5, layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                mPresenter.loadMovies()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appComponent = (activity?.application as App).appComponent
        appComponent.inject(this)
        appComponent.inject(mPresenter)

        mPresenter.setView(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mPresenter.loadMovies()
    }

    override fun showProgressView(show: Boolean) {
        swipeRefereshLayout.isRefreshing = show
    }

    override fun updateList() {
        mAdapter.notifyDataSetChanged()
    }

    override fun resetScrollListener() {
        mScrollListener.reset()
    }
}
