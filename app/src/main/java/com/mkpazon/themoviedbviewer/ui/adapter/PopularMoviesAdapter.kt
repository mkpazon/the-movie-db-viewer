package com.mkpazon.themoviedbviewer.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mkpazon.themoviedbviewer.R
import com.mkpazon.themoviedbviewer.model.Movie
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.item_movie_card.view.*

class PopularMoviesAdapter(private val mItems: MutableList<Movie>) : RecyclerView.Adapter<PopularMoviesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularMoviesViewHolder {
        return PopularMoviesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie_card, parent, false))
    }

    override fun onBindViewHolder(holder: PopularMoviesViewHolder, position: Int) {
        val movie = mItems[position]
        holder.itemView.textViewTitle.text = movie.title
        ImageLoader.getInstance().displayImage(movie.imageUrl, holder.itemView.imageViewMovieImg)
        holder.itemView.ratingBar.rating = movie.rating / 10 * 5
    }

    override fun getItemCount(): Int {
        return mItems.size
    }
}

class PopularMoviesViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)